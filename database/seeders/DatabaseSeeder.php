<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Jabatan;
use App\Models\Gaji;
use App\Models\Jabatan;
use App\Models\Absen;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123
            45678')
        ]);
        
       

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        Karyawan::factory(0)->create();
        Shift::factory(0)->create();
        Jabatan::factory(30)->create();
        Jabatan::factory(0)->create();
        Absen::factory(0)->create();
    }
}
