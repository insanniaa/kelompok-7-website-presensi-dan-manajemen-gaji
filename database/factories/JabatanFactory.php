<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\jabatan>
 */
class JabatanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id_jabatan' => $this->faker->ID_jabatan(),
            'nama_jabatan' => $this->faker->nama_jabatan(),
            'gaji_pokok' => $this->faker->gaji_pokok(),
        ];
    }
}
