<?php

namespace Database\Factories;

use App\Models\Karyawan;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Shift;

class ShiftFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            // 'nama' => fake()->word(),
            // 'jabatan' => fake()->jobTitle(),
            'karyawan_id' => fake()->unique()->randomElement(Karyawan::all('id')),
            'hari' => fake()->dayOfWeek(),
            'jam_masuk' => fake()->time(),
            'jam_keluar' => fake()->time()
        ];
    }
}
