<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Karyawan>
 */
class KaryawanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->email(),
            'jabatan' => fake()->jobTitle(),
            'tempat_lahir' => fake()->city(),
            'tanggal_lahir' => fake()->date('Y_m_d'),
            'alamat' => fake()->address(),
            'tanggal_bergabung' => fake()->date('Y_m_d'),
            'nomor_handphone' => fake()->phoneNumber(),
            'nomor_rekening' => fake()->creditCardNumber('Visa'),
        ];
    }
}
