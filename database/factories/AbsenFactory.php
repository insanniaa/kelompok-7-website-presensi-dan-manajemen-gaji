<?php

namespace Database\Factories;
use App\Models\Absen;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Karyawan>
 */
class AbsenFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Absen::class;
    public function definition()
    {
        return [
            'nama' => $this->faker->name,
            'shift' => $this->faker->randomElement(['Pagi', 'Sore', 'Malam']),
            'tgl_absen' => $this->faker->date,
            'kedatangan' => $this->faker->time,
            'pulang' => $this->faker->optional()->time,
            'keterangan' => $this->faker->sentence,
        ];
    }
}

