@extends('layouts.main')


@section('container')
    <p class="text-2xl mb-4 md:me-24">Welcome <span class="font-semibold ">{{ Auth::user()->name }}</span></p>
    <div class="flex flex-col justify-between gap-6">

        <a href="/karyawan" class="">


            <div class="max-w-4xl h-96 p-6 bg-white border border-gray-200 rounded-xl shadow flex flex-row justify-between">

                <div class="flex flex-col w-full justify-between">
                    <div class="flex flex-col gap-6">
                        <h5 class="align text-xl font-semibold tracking-tight text-gray-900 ">Total Employees</h5>

                        <p class="text-7xl font-bold text-gray-700 justify-self-center">{{ $data_karyawan->count() }}
                        </p>
                    </div>

                    <a href="#"
                        class="w-fit inline-flex max-w-xs items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        View more
                        <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 14 10">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9" />
                        </svg>
                    </a>
                </div>


                <div class="w-full relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    No
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Nama Karyawan
                                </th>

                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($data_karyawan as $karyawan)
                                <tr
                                    class="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50 even:dark:bg-gray-800 border-b dark:border-gray-700">
                                    <th scope="row"
                                        class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $loop->iteration }}
                                    </th>
                                    <th scope="row"
                                        class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $karyawan->name }}
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="flex justify-center mt-6">

                    </div>
                </div>
            </div>
        </a>

        <div class="flex flex-row justify-between max-w-4xl">
            <a href="#">
                <div
                    class="w-full mr-6 ml-0 p-6 bg-white border border-gray-200 rounded-lg shadow flex flex-row justify-between">

                    <h5 class="mb-2 text-2xl font-semibold tracking-tight text-gray-900">Absen</h5>

                    <a href="#"
                        class="inline-flex max-w-xs items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        View more
                        <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 14 10">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9" />
                        </svg>
                    </a>

                </div>
            </a>

            <a href="#">
                <div class="w-full p-6 bg-white border border-gray-200 rounded-lg shadow flex flex-row justify-between">

                    <h5 class="mb-2 text-2xl font-semibold tracking-tight text-gray-900 ">Gaji</h5>

                    <a href="#"
                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">
                        View more
                        <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 14 10">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9" />
                        </svg>
                    </a>
                </div>
            </a>

        </div>

    </div>
@endsection
